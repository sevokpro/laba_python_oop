class account_factory(object):   
	def __init__(self, balanse = 500):
		self.__balanse = balanse

	@property
	def balanse(self):
		return self.__balanse

	@balanse.setter
	def balanse(self, value):
		if (value < 0):
			self.error = 'too low balanse for operation'
		else:
			self.error = 'no errors'
			self.__balanse = value


	def withdraw(self, amount):
		self.balanse += amount

class bank_factory(object):
	def __init__(self):
		self.accounts = []

	def run(self):
		choice = 0
		message = '-----------------\ninput key:\n'
		message += '1: create_account\n'
		message += '2: print_accounts\n'
		message += '3: withdraw from account with id 0, 200 value\n'
		message += '4: close\n'
		choices = {
			1 : lambda : self.create_account(),
			2 : lambda : self.print_accounts(),
			3 : lambda : self.withdraw(0, 200),
			4 : lambda : exit(),
		}
		# check for correct input
		while not(choice in choices):
			choice = input(message + '----------\nchoice: ')
		else:
			choices[choice]()
	def create_account(self):
		acc = account_factory()
		self.accounts.append(acc)
		self.run()

	def withdraw(self, id, amount):
		self.accounts[id].withdraw(amount)
		self.run()

	def print_accounts(self):
		for i in range(0, len(self.accounts)):
			print ("account id: " + str(i) + " account balanse: " + str(self.accounts[i].balanse))
		self.run()		

def main():
	bank = bank_factory()
	bank.run()

if __name__ == '__main__':
	main()