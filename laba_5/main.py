class account(object):   
	def __init__(self, balanse = 500):
		self.__balanse = balanse
		
	@property
	def balanse(self):
		return self.__balanse
	#this is check not null balanse in property
	@balanse.setter
	def balanse(self, value):
		if (value < 0):
			self.error = 'too low balanse for operation'
		else:
			self.error = 'no errors'
			self.__balanse = value


	def withdraw(self, amount):
		self.balanse += amount


def main():
	acc_1 = account()
	#__balanse private! but balanse property balanse public and show you current balanse
	print acc_1.balanse
	acc_1.withdraw(500)
	print acc_1.balanse
	print acc_1.error
	#all ok
	print '---------'
	acc_1.withdraw(-2000)
	print acc_1.balanse
	print acc_1.error

if __name__ == '__main__':
	main()