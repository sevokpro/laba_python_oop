import sys
from PyQt4 import QtGui, uic, QtCore

class account_factory(object):   
	def __init__(self, balanse = 500):
		self.__balanse = balanse

	@property
	def balanse(self):
		return self.__balanse

	@balanse.setter
	def balanse(self, value):
		if (value < 0):
			self.error = 'too low balanse for operation'
		else:
			self.error = 'no errors'
			self.__balanse = value


	def withdraw(self, amount):
		self.balanse += amount

class bank_factory(object):
	def __init__(self):
		self.accounts = []

	def create_account(self):
		acc = account_factory()
		self.accounts.append(acc)

	def withdraw(self, id, amount):
		self.accounts[id].withdraw(amount)

	def get_accounts(self):
		data = []
		for i in range(0, len(self.accounts)):
			data.append({'id': i, 'balanse': self.accounts[i].balanse})
		
		return data

	def print_accounts(self):
		for i in range(0, len(self.accounts)):
			print ("account id: " + str(i) + " account balanse: " + str(self.accounts[i].balanse))

class BankWindow(QtGui.QMainWindow):
	def __init__(self):
		# super(BankWindow, self).__init__()
		# self.bank = bank_factory()
		# self.show()
		self.app = QtGui.QApplication(sys.argv)
 		
 		self.widget = QtGui.QWidget()
 		self.widget.resize(300, 300)
 		self.widget.setWindowTitle('simple')
 		self.widget.show()
 		
		sys.exit(self.app.exec_())
		

class Window(QtGui.QWidget):
	def __init__(self, parent=None):
		self.model = bank_factory()
		self.model.create_account()
		self.model.create_account()
		self.model.create_account()

		QtGui.QWidget.__init__(self, parent)
		self.width = 500;
		self.height = 500;
	   	self.setGeometry(100, 100, self.width, self.height)
	   	self.setWindowTitle('Bank')	
	   	
	   	# quit = QtGui.QPushButton('Close', self)
	   	# quit.setGeometry(self.width - 100, self.height- 30, 70, 20)	
	   	
	   	label = QtGui.QLabel('Actions:', self)
	   	label.setGeometry(30, 50, 200, 10)

		create_account = QtGui.QPushButton('create_account', self)
		create_account.setGeometry(30, 100, 150, 40)

		print_accounts = QtGui.QPushButton('print_accounts', self)
		print_accounts.setGeometry(30, 150, 150, 40)

		withdraw = QtGui.QPushButton('withdraw', self)
		withdraw.setGeometry(30, 200, 150, 40)

		withdraw_id_label = QtGui.QLabel('id:', self)
		withdraw_id_label.setGeometry(30, 250, 50, 40)

		self.withdraw_id = QtGui.QLineEdit('1', self)
		self.withdraw_id.setGeometry(50, 250, 50, 40)

		withdraw_id_label = QtGui.QLabel('value:', self)
		withdraw_id_label.setGeometry(110, 250, 50, 40)

		self.withdraw_value = QtGui.QLineEdit('200', self)
		self.withdraw_value.setGeometry(150, 250, 50, 40)

		close = QtGui.QPushButton('close', self)
		close.setGeometry(30, 300, 150, 40)
	   	
		self.view = QtGui.QLabel('', self)
		self.view.setWordWrap(True)
	   	self.view.setGeometry(300, 50, 200, 400)


	   	self.connect(close, QtCore.SIGNAL('clicked()'), QtGui.qApp, QtCore.SLOT('quit()'))

	   	self.connect(create_account, QtCore.SIGNAL('clicked()'), self.create_acc)
	   	self.connect(print_accounts, QtCore.SIGNAL('clicked()'), self.print_accounts)
	   	self.connect(withdraw, QtCore.SIGNAL('clicked()'), self.withdraw)

	def create_acc(self):
		self.model.create_account()

	def print_accounts(self):
		data = self.model.get_accounts()
		text = ''
		for item in data:
			text += 'id: ' + str(item['id']) + ' balanse: ' + str(item['balanse']) + '\n'

		self.view.setText(text)

	def withdraw(self):
	 	id = self.withdraw_id.text()
	 	value = self.withdraw_value.text()
	 	self.model.withdraw(int(id), int(value))



def main():
	bank = bank_factory()
	bank.create_account()
	bank.print_accounts()
	print bank.get_accounts()

if __name__ == '__main__':
	# main()
	
	app = QtGui.QApplication(sys.argv)
	bank = Window()
	bank.show()
	sys.exit(app.exec_())