class account_factory(object):   
	def __init__(self, balanse = 500):
		self.__balanse = balanse

	@property
	def balanse(self):
		return self.__balanse
	#this is check not null balanse in property
	@balanse.setter
	def balanse(self, value):
		if (value < 0):
			self.error = 'too low balanse for operation'
		else:
			self.error = 'no errors'
			self.__balanse = value


	def withdraw(self, amount):
		self.balanse += amount

class bank_factory(object):
	#created bank factory, what containt accounts and can print all accounts
	def __init__(self):
		self.accounts = []

	def create_account(self):
		acc = account_factory()
		self.accounts.append(acc)

	def print_accounts(self):
		for i in range(0, len(self.accounts)):
			print ("account id: " + str(i) + " account balanse: " + str(self.accounts[i].balanse))
		

def main():
	bank = bank_factory()
	# created 2 accounts
	bank.create_account()	
	bank.create_account()	
	bank.print_accounts()
	print ('------------')
	# withdraw for change second account
	bank.accounts[0].withdraw(100)
	bank.print_accounts()

if __name__ == '__main__':
	main()